const express = require("express");
var cors = require('cors')
var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('keys/example.key', 'utf8');
var certificate = fs.readFileSync('keys/example.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

const app = express();
app.use(cors());
const { createProxyMiddleware } = require('http-proxy-middleware');
app.use('/api', createProxyMiddleware({ 
    target: 'http://localhost:8080/', //original url
    changeOrigin: true, 
    //secure: false,
    onProxyRes: function (proxyRes, req, res) {
       proxyRes.headers['Access-Control-Allow-Origin'] = '*';
    }
}));
//app.listen(5000);

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);


httpServer.listen(5000);
httpsServer.listen(5005);

